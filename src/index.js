const config = require('./config.js')
const logger = require('./lib/logger.js')

const fastify = require('./lib/server.js')

async function main() {
    await fastify.listen(config.fastify.listen)
}

main()
    .then(function () {
        logger.info(`listening on port ${config.fastify.listen.port}`)
        process.on('SIGINT', function (){
            logger.info(`SIGINT caught`)
            fastify.close()
                .then()
                .catch()
            process.exit(0)
        })
    })
    .catch(function (err) {
        logger.error(err.message)
        logger.debug(err.stack)
        process.exit(1)
    })