const fs = require('fs').promises
const util = require('node:util')

const axios = require('axios')
const fastifyFactory = require('fastify')

const config = require('../config.js')
const logger = require('../lib/logger.js')

const fastify = fastifyFactory(config.fastify.factory)

fastify.get('/', async (request, reply) => {

    let html = ''

    try {
        // get HTML "template" from file
        const htmlTemplate = (await fs.readFile('./views/slash.html')).toString()
        // encode the requested URL
        const encodedUrl = encodeURIComponent(request.url)
        // get API key from config file
        const apiKey = config.builder.apiKey
        // construct builder URL
        const builderURL = `https://cdn.builder.io/api/v1/html/page?apiKey=${apiKey}&url=${encodedUrl}`
        // HTTP GET the builder HTML
        const builderResponse = await axios.get(builderURL)
        // interpolate the builder HTML into the template HTML
        const builderHTML = builderResponse.data.data.html
        html = util.format(htmlTemplate, builderHTML)
    } catch (err) {
        logger.error(`error rendering page: ${err.message}`)
        logger.debug(err.stack)
    }

    return reply.type('text/html').send(html)
})

module.exports = fastify