const pinoFactory = require('pino')

const config = require('../config.js')

const logger = pinoFactory(config.pino)

module.exports = logger