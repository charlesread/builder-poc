const config = {

    builder: {
        apiKey: process.env['BUILDER_API_KEY'] || ''
    },
    fastify: {
        factory: {
        },
        listen: {
            host: '0.0.0.0',
            port: 3000
        }
    },
    pino: {
        level: 'debug',
        transport: {
            target: 'pino-pretty',
            options: {
                translateTime: 'SYS:standard'
            }
        }
    }

}

module.exports = config