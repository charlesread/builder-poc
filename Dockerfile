FROM node:18-bullseye

RUN mkdir -p /usr/src/app

ADD src /usr/src/app

WORKDIR /usr/src/app

RUN rm -rf node_modules

RUN npm install

CMD ["node", "index.js"]