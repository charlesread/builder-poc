# builder.io - POC

builder.io is a "headless" Content Management System (CMS). The paradigm is a bit interesting, in effect all one must do is render a big chunk of client-side HTML/JS/CSS etc. in your application, which effectively communicates with the builder.io web service/application, that then renders all of your actual content.

So the lifecycle of a request to a builder.io-ified application is as follows:

1. The web server receives a request.
2. The server performs an HTTP GET to builder.io, which contains an API key and the URL being requested.
3. The payload from builder.io contains all of the HTML necessary to render that page.
4. The web server simply interpolates this data into a basic HTML template and sends that to the browser.
5. The rendered page is then communicating with the builder.io web service to actually render the content.

Basically, our web application serves as a shell, into which the builder.io "application" is injected, and all of the application's actual content is stored in builder.io.

## builder.io Configuration Overview

builder.io exposes the concept of `model`s and `content`s.

`model`s have, among other things;

- a preview URL (such as `http://localhost:3000`)

`content`s have, among other things;

- a page URL (such as `/` or `/about`)

## Running

### builder.io Pre-Requisites

builder.io makes this somewhat of a synchronous process. In that you first have to create the "model" and "content" in builder.io before your local application will even function.  One does this in the builder.io web console.

This application exposes only a single route at `/`, and it by default runs at `http://localhost:3000`. So to run this application you need to have a `model`, of type `page`, that has a preview URL of `http://localhost:3000`, that has a `content` whose page URL is `/`.

### Running this application

#### A note about "secret" configuration parameters

Standard practice these days is to have privileged configuration parameters stored as environment variables, particularly in containers. This project follows that practice. 

There are _many_ ways to pass environment variables to processes. So you'll see this below, on *nix systems you can simply prepend key=value pairs to the command. With Docker one uses the `-e` flag.

Now for the easy part.

#### On a *nix machine with Node installed

```shell
$ git clone <repo>
$ cd builder-poc/src
$ npm install
$ BUILDER_API_KEY=abc123 node index.js
# or
$ node index.js # input BUILDER_API_KEY into config.js
# send SIGINT to terminate, CTRL-C on most machines****
```

Now visit `http://localhost:3000` in a browser.

#### On anything, as a Docker container

```shell
git clone <repo>
cd builder-poc
docker build -t builder-poc .
docker run -it --rm -p 3000:3000 --name builder-poc -e BUILDER_API_KEY=abc123 builder-poc
# send SIGINT to terminate, CTRL-C on most machines
```

Now visit `http://localhost:3000` in a browser.


